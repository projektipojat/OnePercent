extends CanvasLayer
var magazinetext
var ammunitiontext
var healthtext
var messagetext
var weapontext
var time

func _ready():
	magazinetext = 0
	ammunitiontext = 0
	healthtext = 0
	messagetext = ""
	weapontext = ""
	time = ""

func _process(delta):
	$Weapon.text = str(weapontext)
	$Ammunition.text = "Ammo: " + str(magazinetext) + "/" + str(ammunitiontext)
	$HP.text = "Health: " + str(healthtext)
	$Message.text = str(messagetext	)
	$Time.text = str(time)
	pass


func _on_Restart_pressed():
	get_tree().reload_current_scene()


func _on_Exit_pressed():
	get_tree().quit()
