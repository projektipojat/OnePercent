extends Node

var magazine = 10;
var ammunition = 30;
var weapon_name = "Hand Gun"
var can_shoot = true


var bullet = preload("res://models/particles/bullet/Bullet.tscn")

func shoot(pos, mouse_position, enemy):
	if can_shoot:
		var bullet_instance = bullet.instance()
		bullet_instance.BASE_DAMAGE = 20
		bullet_instance.CRIT_DAMAGE = 200
		bullet_instance.position.y = pos.y
		if mouse_position.x > 0:
			bullet_instance.position.x = pos.x 
		else:
			bullet_instance.position.x = pos.x 
		add_child(bullet_instance)
		bullet_instance.shoot(mouse_position, enemy)
		magazine -= 1
		$Shoot.play()

func reload():
	if ammunition > 0:
		can_shoot = false
		$Reload.play()

func empty():
	$Empty.play()

func _on_Reload_finished():
	if (ammunition >= 10):
		magazine = 10;
		ammunition -= 10
		can_shoot = true
	elif (ammunition < 10 and ammunition > 0):
		magazine = ammunition
		ammunition = 0
		can_shoot = true
