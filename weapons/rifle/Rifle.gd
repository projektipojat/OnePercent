extends Node

var magazine = 30;
var ammunition = 120;
var weapon_name = "Assault Rifle"
var can_shoot = true

var bullet = preload("res://models/particles/bullet/Bullet.tscn")

func shoot(pos, mouse_position, enemy):
	if can_shoot:
		var bullet_instance = bullet.instance()
		bullet_instance.BASE_DAMAGE = 40
		bullet_instance.CRIT_DAMAGE = 400
		bullet_instance.position.y = pos.y
		if mouse_position.x > 0:
			bullet_instance.position.x = pos.x 
		else:
			bullet_instance.position.x = pos.x 
		add_child(bullet_instance)
		bullet_instance.shoot(mouse_position, enemy)
		magazine -= 1
		$Shoot.play()

func reload():
	if ammunition > 0:
		can_shoot = false
		$Reload.play()
		pass

func empty():
	$Empty.play()
	pass

func _on_Reload_finished():
	if (ammunition >= 10):
		magazine = 30;
		ammunition -= 30
		can_shoot = true
	elif (ammunition < 30 and ammunition > 0):
		magazine = ammunition
		ammunition = 0
		can_shoot = true
