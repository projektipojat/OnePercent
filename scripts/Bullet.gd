extends RigidBody2D

var VELOCITY = 1200
var vel = Vector2(100, 0)
var BASE_DAMAGE = 1
var CRIT_DAMAGE = BASE_DAMAGE * 10
var ENEMY = false

func _ready():
	connect("body_entered", self, "contact")
	$Timer.start()

func shoot(var dir, var enemy):
	ENEMY = enemy
	if enemy:
		set_collision_layer_bit(4, 0)
		set_collision_layer_bit(6, 1)
		print(get_collision_layer_bit(7))
	set_linear_velocity(dir * VELOCITY)

func contact(var body):
	destroy()

func destroy():
	queue_free()

func _on_Timer_timeout():
	destroy()
	pass
