extends RayCast2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _process(delta):
	var space_state = get_world_2d().direct_space_state
	var result = space_state.intersect_ray(Vector2(0,800), Vector2(800, 800))
	print(result)

