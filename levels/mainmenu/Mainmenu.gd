extends Sprite

func _on_New_Game_pressed():
	get_tree().change_scene("res://levels/Level.tscn")

func _on_Quit_pressed():
	get_tree().quit()
