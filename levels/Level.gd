extends TileMap

var magazinecount
var ammunitioncount
var weaponname
var healthbar
var time

var cursor = load("res://hud/images/cursor.png")

func _ready():
	pass

func _process(delta):
	magazinecount = $Player.current_weapon.magazine
	ammunitioncount = $Player.current_weapon.ammunition
	weaponname = $Player.current_weapon.weapon_name
	healthbar = $Player.health
	time = $Player.elapsed_time
	
	$Hud.weapontext = weaponname
	$Hud.magazinetext = magazinecount
	$Hud.ammunitiontext = ammunitioncount
	$Hud.healthtext = healthbar
	$Hud.time = time
	pass	

func _on_Area2D_body_entered(body):
	$Player.health = 0

func _on_Goal_body_entered(body):
	$Player.timer_on = false
	$Hud.messagetext = "Victory!"
	get_tree().change_scene("res://levels/mainmenu/Gongrats.tscn")


func _on_Player_game_over():
	$Player.disabled_controls = true
	$Hud/AnimationPlayer.play("game over")
	print("game over")
