extends RigidBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	get_node("Area2D").connect("body_entered", self, "contact")

func contact(var body):
	if(body.is_in_group("player")):
		var rand = (randi()%10)
		body.pistol_instance.ammunition += rand+5
		queue_free()
		
func _process(delta):
	pass


