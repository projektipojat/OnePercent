extends RigidBody2D

func _ready():
	get_node("Area2D").connect("body_entered", self, "contact")
	pass
	
func contact(var body):
	if (body.is_in_group("player")):
		if body.health < 100:
			body.health += 10
		queue_free()

