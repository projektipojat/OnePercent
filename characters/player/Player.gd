extends KinematicBody2D

const GRAVITY = 980

const FLOOR_ANGLE_TOLERANCE = 40
const WALK_FORCE = 1000
const WALK_MIN_SPEED = 100
var WALK_MAX_SPEED = 400
const STOP_FORCE = 2500
const JUMP_SPEED = 350
const JUMP_MAX_AIRBORNE_TIME = 0.2

const SLIDE_STOP_VELOCITY = 1.0
const SLIDE_STOP_MIN_TRAVEL = 1.0

var velocity = Vector2()
var on_air_time = 100
var jumping = false

# Player stats
var health = 100
var live = true
var disabled_controls = false

var prev_jump_pressed = false

var pistol = preload("res://weapons/pistol/Pistol.tscn")
var rifle = preload("res://weapons/rifle/Rifle.tscn")
var pistol_instance
var rifle_instance
var mouse_position
var current_weapon
var time_start
var time_now
var elapsed_time
var timer_on = true
var crouching = false

signal game_over

func _ready():
	time_start = OS.get_unix_time()
	
	pistol_instance = pistol.instance()
	rifle_instance = rifle.instance()
	add_child(pistol_instance)
	add_child(rifle_instance)
	
	current_weapon = pistol_instance
	
	$Camera2D.make_current()
	$Camera2D.offset = Vector2(200, 0)

func _physics_process(delta):
	var force = Vector2(0, GRAVITY)
	var jump = Input.is_action_pressed("jump")
	var shoot = Input.is_action_just_pressed("shoot")
	var reload = Input.is_action_just_pressed("reload")
	var crouch = Input.is_action_pressed("crouch")
	var zoom = Input.is_action_pressed("zoom")
	
	if timer_on:
		time_now = OS.get_unix_time()
		elapsed_time = time_now - time_start
	
		var minutes = elapsed_time / 60
		var seconds = elapsed_time % 60
		elapsed_time = "%02d : %02d" % [minutes, seconds]

	var stop = true
	
	
	if Input.is_action_pressed("pistol") and not disabled_controls:
		current_weapon = pistol_instance
	
	if Input.is_action_pressed("rifle") and not disabled_controls:
		current_weapon = rifle_instance

	if Input.is_action_pressed("ui_left") and not disabled_controls:
		if velocity.x <= WALK_MIN_SPEED and velocity.x > -WALK_MAX_SPEED:
			force.x -= WALK_FORCE
			stop = false
	
	elif Input.is_action_pressed("ui_right") and not disabled_controls:
		if velocity.x >= -WALK_MIN_SPEED and velocity.x < WALK_MAX_SPEED:
			force.x += WALK_FORCE
			stop = false
	
	if crouch and not disabled_controls:
		crouching = true
		$AnimatedSprite.animation = "crouch"
		$Area2D/CollisionShape2D.disabled = true
		$CrouchArea/CollisionShape2D.disabled = false
	else:
		crouching = false
		$Area2D/CollisionShape2D.disabled = false
		$CrouchArea/CollisionShape2D.disabled = true
	
	if shoot and not disabled_controls:
		if (current_weapon.magazine > 0):
			
			if crouch:
				mouse_position = Vector2(get_local_mouse_position().x, get_local_mouse_position().y - 20).normalized()
				current_weapon.shoot(Vector2(position.x, position.y + 20), mouse_position, false)
			else:
				mouse_position = get_local_mouse_position().normalized()
				current_weapon.shoot(position, mouse_position, false)
		else:
			current_weapon.empty()
	if reload and not disabled_controls:
		current_weapon.reload()
		
	if zoom and current_weapon == rifle_instance and not disabled_controls:
		$Camera2D.offset = Vector2(200 + get_local_mouse_position().x / 5, get_local_mouse_position().y / 5)
	else:
		$Camera2D.offset = Vector2(200, 0)
	
	if stop:
		var vsign = sign(velocity.x)
		var vlen = abs(velocity.x)

		vlen -= STOP_FORCE * delta
		if vlen < 0:
			vlen = 0

		velocity.x = vlen * vsign
		if abs(velocity.x) < WALK_MIN_SPEED and not crouch and current_weapon == pistol_instance:
			$AnimatedSprite.animation = "idle"
		if abs(velocity.x) < WALK_MIN_SPEED and not crouch and current_weapon == rifle_instance:
			$AnimatedSprite.animation = "idle_rifle"

	velocity += force * delta
	if(velocity.x != 0 and not jumping and current_weapon == pistol_instance):
		$AnimatedSprite.animation = "walk"
		$AnimatedSprite.flip_h = velocity.x < 0
		$AnimatedSprite.play()
	if(velocity.x != 0 and not jumping and current_weapon == rifle_instance):
		$AnimatedSprite.animation = "rifle_walk"
		$AnimatedSprite.flip_h = velocity.x < 0
		$AnimatedSprite.play()
	velocity = move_and_slide(velocity, Vector2(0, -1))

	if is_on_floor():
		on_air_time = 0

	if jumping and velocity.y > 0:
		jumping = false
	
	if jumping:
		$AnimatedSprite.animation = "jump"
		$AnimatedSprite.flip_h = velocity.x < 0
	
	if velocity.y > 5:
		$AnimatedSprite.animation = "fall"
		$AnimatedSprite.flip_h = velocity.x < 0

	if on_air_time < JUMP_MAX_AIRBORNE_TIME and jump and not prev_jump_pressed and not jumping and not disabled_controls:
		velocity.y = -JUMP_SPEED
		jumping = true
	
	if health <= 0 and live:
		$AnimatedSprite.animation = "idle"
		$AnimatedSprite.rotation_degrees = -90
		$AnimatedSprite.offset = Vector2(-80, 0)
		live = false
		emit_signal("game_over")

	on_air_time += delta
	prev_jump_pressed = jump




func _on_Area2D_body_entered(body):
	if body.is_in_group("bullet"):
		if body.ENEMY:
			health -= body.BASE_DAMAGE
			body.queue_free()
