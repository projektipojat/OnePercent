extends KinematicBody2D

var health = 200

var shooting = false
var can_shoot = true
var spotted = false

var bullet = preload("res://models/particles/bullet/Bullet.tscn")
var player = preload("res://characters/player/Player.tscn")

func _ready():
	$AnimatedSprite.animation = "basic"

func _process(delta):
	if !spotted:
		$AnimatedSprite.play()
	else:
		var player_position = (get_parent().get_node("Player").position - position).normalized()
		$AnimatedSprite.rotation_degrees = rad2deg(atan(player_position.y / player_position.x))
	
	if shooting and can_shoot:
		var player_position = (get_parent().get_node("Player").position - 
		position).normalized()
		var bullet_instance = bullet.instance()
		bullet_instance.position.y = position.y-16
		if (player_position.x > 0):
			bullet_instance.position.x = position.x + 35
		else:
			bullet_instance.position.x = position.x -35
		get_parent().add_child(bullet_instance)
		bullet_instance.BASE_DAMAGE = 11
		bullet_instance.shoot(player_position, true)
		$Turret_shot.play()
		$Timer.start()
		can_shoot = false
		
func hit(var damage):	
	health -= damage

func _on_Body_body_entered(body):
	if (body.is_in_group("bullet")):
		body.queue_free()
		health -= body.BASE_DAMAGE
		if (health <= 0):
			get_node("AnimationPlayer").play("death")
			can_shoot = false
			$Timer.stop()
			$DetectPlayer/DetectShape.disabled = true
			$Body/Bodycollider.disabled = true
			$CollisionShape2D.disabled = true

	
func _on_DetectPlayer_body_entered(body):
	if (body.is_in_group("player")):
		$AnimatedSprite.animation = "shooting"
		spotted = true
		shooting = true

func _on_DetectPlayer_body_exited(body):
	if (body.is_in_group("player")):
		shooting = false
		spotted = false
		$AnimatedSprite.animation = "basic"

func _on_Timer_timeout():
	can_shoot = true
