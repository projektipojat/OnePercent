extends KinematicBody2D

const GRAVITY = 1000.0
const WALK_FORCE = 1000

var velocity = Vector2()
var health = 100

var shooting = false
var can_shoot = true
var spotted = false

var medkit = preload("res://models/particles/medkit/Medkit.tscn")
var bulletcrate = preload ("res://models/particles/bulletcrate/Bulletcrate.tscn")
var bullet = preload("res://models/particles/bullet/Bullet.tscn")
var player = preload("res://characters/player/Player.tscn")

func _ready():
	pass

func _physics_process(delta):
	var force = Vector2(0, GRAVITY)	
	velocity += force * delta
	velocity = move_and_slide(velocity, Vector2(0, -1))
	
func _process(delta):
	
	if !spotted:
		$AnimatedSprite.play("idle")
	else:
		var player_position = (get_parent().get_node("Player").position - 
		position).normalized()	
		$AnimatedSprite.play("idle_spotted")
		if player_position.x < 0: 
			$AnimatedSprite.set_flip_h(true)
		else:
			$AnimatedSprite.set_flip_h(false)
			
	if shooting and can_shoot:
		var player_position = (get_parent().get_node("Player").position - 
		position).normalized()
		var bullet_instance = bullet.instance()
		bullet_instance.position.y = position.y
		if (player_position.x > 0):
			bullet_instance.position.x = position.x + 35
		else:
			bullet_instance.position.x = position.x -35
		get_parent().add_child(bullet_instance)
		bullet_instance.BASE_DAMAGE = 35 
		bullet_instance.shoot(player_position, true)
		$PistolFire.play()
		$Timer.start()
		can_shoot = false

func hit(var damage):	
	if health > 0:
		health -= damage

func create_drops():
	var rand = (randi()%4)
	if (rand == 0):
		var medkit_instance = medkit.instance()
		medkit_instance.position.y = position.y
		medkit_instance.position.x = position.x
		get_parent().add_child(medkit_instance)	
	if (rand == 3):
		var bulletcrate_instance = bulletcrate.instance()
		bulletcrate_instance.position.y = position.y
		bulletcrate_instance.position.x = position.x
		get_parent().add_child(bulletcrate_instance)	

func enemy_die():	
	create_drops()
	queue_free()

func _on_Body_body_entered(body):
	if body.is_in_group("bullet"):
		body.queue_free()
		health -= body.BASE_DAMAGE
		if (health <= 0):
			can_shoot = false
			shooting = false
			$AnimatedSprite.hide()
			get_node("Animation/AnimationPlayer").play("bodyshot")
			print("fuck")
			$Body/Bodycollider.disabled = true
			$Head/Headcollider.disabled = true

func _on_Head_body_entered(body):
	if body.is_in_group("bullet"):
		body.queue_free()
		health -= body.CRIT_DAMAGE
		if (health <= 0):
			can_shoot = false
			shooting = false
			$AnimatedSprite.hide()
			get_node("Animation/AnimationPlayer").play("headshot")
			$Body/Bodycollider.disabled = true
			$Head/Headcollider.disabled = true


func _on_AnimationPlayer_animation_finished(anim_name):
	enemy_die()

func _on_DetectPlayer_body_entered(body):	
	if (body.is_in_group("player")):		
		shooting = true
		spotted = true

func _on_DetectPlayer_body_exited(body):
	if (body.is_in_group("player")):
		shooting = false
		spotted = false

func _on_Timer_timeout():
	can_shoot = true
	pass # replace with function body
